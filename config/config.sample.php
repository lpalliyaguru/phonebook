<?php
define('DEV_MODE', true);
define('APP_ROOT', '');//Application root directory
define('APP_FILE_UPLOAD_PATH', APP_ROOT.'/uploads/');
define('APP_TEMPLATE_PATH', APP_ROOT.'/views');
define('DB_NAME','');
define('DB_HOST','');
define('DB_USER','');
define('DB_PASSWORD','');
define('APP_URL','http://phonebook.local');
define('APP_CDN_PATH','http://phonebook.local');

define('APP_MONGO_DB_NAME','');
define('APP_MONGO_DB_HOST','');
define('APP_MONGO_DB_PORT','');
define('APP_MONGO_DB_USER','');
define('APP_MONGO_DB_PASSWORD','');

define('APP_LOCALE_PATH', APP_ROOT.'/locale/');

define('APP_MAIL_FROM_MAIL','');
define('APP_MAIL_FROM_NAME','');
define('APP_MAIL_REPLYTO','');
define('APP_MAIL_RETURN_PATH','');
define('APP_MAIL_SENDER_MASK','');
define('APP_MAIL_SENDER_NAME','');

define('APP_FACEBOOK_APP_ID','');
define('APP_FACEBOOK_APP_SECRET', '');
define('APP_SMTP_UNAME','');
define('APP_SMTP_PASSWORD','');
define('APP_SMTP_HOST','');
define('APP_SMTP_PORT',465);

?>