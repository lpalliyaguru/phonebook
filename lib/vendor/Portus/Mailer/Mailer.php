<?php
namespace Portus\Mailer;
use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use Portus\Db\Db;
use Portus\Factory\Object;
/**
 * 
 * @ODM\Document
 *
 */
class Mailer {
	/**
	 * 
	 * @ODM\Id
	 */
	private $id;
	/**
	 * 
	 * @ODM\String
	 */
	private $to;
	/**
	 *
	 * @ODM\String
	 */
	private $cc;
	/**
	 *
	 * @ODM\String
	 */
	private $bcc;
	/**
	 * 
	 * @ODM\Hash
	 */
	private $data;
	/**
	 *
	 * @ODM\String
	 */
	private $body;
	/**
	 *
	 * @ODM\Boolean
	 */
	private $isHtml;
	/**
	 * 
	 * @ODM\String
	 */
	private $hash;
	/**
	 * 
	 * @ODM\Float
	 */
	private $queued;
	/**
	 * 
	 * @ODM\String
	 */
	private $template;	
	/**
	 *
	 * @ODM\String
	 */
	private $subject;
	/**
	 *
	 * @ODM\Boolean
	 */
	private $sent = false;

	public static function getUnsentEmails() {
		$documentManager = Object::getDocumentManager();
		$unsentMails = $documentManager->getRepository('Portus\\Mailer\\Mailer')->findBy(array('sent' => false));
		return $unsentMails;
	}
	
	public function getId() {
		return $this->id;
	}
	
	public function setId($id) {
		$this->id = $id;
		return $this;
	}
	
	public function getToEmail() {
		return $this->to;	
	}
	
	public function setToEmail($email) {
		$this->to = $email;
		return $this;
	}
	
	public function getCC() {
		return $this->cc;
	}
	
	public function setCC($cc) {
		$this->cc = $cc;
		return $this;
	}
	
	public function getBCC() {
		return $this->bcc;
	}
	
	public function setBCC($bcc) {
		$this->bcc = $bcc;
		return $this;
	}
	
	public function getBodyData() {
		return $this->data;
	}
	
	public function setBodyData($body) {
		$this->data = $body;
		return $this;
	}
	
	public function getBodyText() {
		return $this->body;
	}
	
	public function setBodyText($text) {
		$this->body = $text;
		return $this;
	}
	
	public function isHtml() {
		if((bool)func_num_args()) {
			$this->isHtml = func_get_arg(0);
			return $this;
		}
		return $this->isHtml;
	}
	
	public function isSent() {
		if((bool)func_num_args()) {
			$this->sent = func_get_arg(0);
			return $this;
		}
		return $this->sent;
	}
	
	public function getHash() {
		return $this->hash;
	}
	
	public function setHash($hash) {
		$this->hash = $hash;
		return $this;
	}
	
	public function getQueuedTime() {
		return $this->queued;
	}
	
	public function setQueuedTime($time) {
		$this->queued = $time;
		return $this;
	}
	
	public function getTemplate() {
		return $this->template;
	}
	
	public function setTemplate($tpl) {
		$this->template = $tpl;
		return $this;
	}
	
	public function getSubject() {
		return $this->subject;
	}
	
	public function setSubject($subject) {
		$this->subject = $subject;
		return $this;
	}
}