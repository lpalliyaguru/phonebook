<?php
namespace Portus\Mailer;
use PHPMailer\PHPMailer;
use Portus\Mailer\Mailer;
use Portus\Log\Log;

class MailSender {
	
	public function send(Mailer $mailer) {
		
		$phpMailer = new PHPMailer;
		$phpMailer->isSMTP(true);
		$phpMailer->Host = APP_SMTP_HOST;
		$phpMailer->Port = APP_SMTP_PORT;
		$phpMailer->SMTPAuth = true;
		$phpMailer->SMTPSecure = 'ssl';
		$phpMailer->Username = APP_SMTP_UNAME;
		$phpMailer->Password = APP_SMTP_PASSWORD;
		$phpMailer->Subject = $mailer->getSubject();
		$phpMailer->Encoding = 'base64';
		$phpMailer->addAddress($mailer->getToEmail());
		$phpMailer->setFrom(APP_MAIL_FROM_MAIL,APP_MAIL_FROM_NAME);

		if($mailer->isHtml()) {
			$bodyContent = self::parseTemplate($mailer->getTemplate(), $mailer->getBodyData());
			$phpMailer->msgHTML($bodyContent);
			include_once APP_ROOT.'/includes/html2text.inc.php';
			$phpMailer->AltBody = convert_html_to_text((string)$bodyContent);
		}	
		else {
			$phpMailer->Body = $mailer->getBodyText();
		}
		
		if($phpMailer->send()) {
			error_log('sent mail to '.$mailer->getToEmail());
			return true;
		}
		else {
			error_log('Unable to sent email to '.$mailer->getToEmail(). ' reason => '.$phpMailer->ErrorInfo);
			return $phpMailer->ErrorInfo;
		}
		
	} 
	
	public static function parseTemplate($template, $data) {
		extract($data);
		ob_start();
		include_once APP_TEMPLATE_PATH.'/mail/'.$template;
		$buffer_contents = ob_get_contents();
		ob_end_clean();
		return $buffer_contents;
	}
}