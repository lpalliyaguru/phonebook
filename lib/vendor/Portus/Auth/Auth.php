<?php
namespace Portus\Auth;
use Weask\User;
use Portus\Factory\Factory;
use Portus\Log\Log;
use Portus\Auth\Device\Device;
use Portus\Auth\Device\DeviceSession;
use Portus\Auth\Device\DeviceSessionException;
use Portus\Factory\Object;
use Doctrine\Common\Collections\Selectable;
use Facebook\FacebookSession;
use Facebook\FacebookRequest;
use Facebook\GraphUser;
class Auth {

	const USER_SIGNIN_SUCCESS = 301;
	const USER_SIGNIN_FAILURE = 302;
	const USER_ALREADY_LOGGED_IN = 303;
	
	const AUTH_KEY_NAME = 'Auth-Token';
	
	const SIGNIN_TYPE_FB = 'fb';
	const SIGNIN_TYPE_EMAIL = 'email';
	const SIGNIN_TYPE_USERNAME = 'username';
	
	public static function setSessionOwner($user) {
		$_SESSION['user'] = array(
								'email' => $user->getEmail(),
								'id' => (string)$user->getId(),
								'name' => $user->getName(),
								'logged_in' => time(),
								'type' => $user->getType()
							);
		return true;
	}
	/**
	 * 
	 * @param Device $device
	 * @param unknown $user_token
	 * @return Gt\User\User
	 */
	public static function getApiSessionOwner(Device $device, $user_token) {
		$activeSession = $device->getActiveSession();
		
		if($activeSession instanceof DeviceSession) {
			if($activeSession->getToken() == $user_token) {
				return $activeSession->getUser();
			}
			return null;
		}
		return null;
	}
	
	public static function getWebSessionOwner() {
		return isset($_SESSION['__session']['owner']) ? User::get($_SESSION['__session']['owner']['id']) : null;
	}
	
	public static function getAuthToken() {
		$headers = getallheaders();
		if(isset($headers['GT-Auth-Token'])) {
			return $headers['GT-Auth-Token'];
		}
		throw new Exception('Authentication token not found');
	}
	
	public static function web_signin($email, $password) {
		$documentManager = Object::getDocumentManager();
		$user = $documentManager->getRepository('Weask\\User')->findOneBy(array('email' => $email));
		if(!$user instanceof User) { throw new \Exception('User does not exist', User::ERROR_USER_NOT_EXIST); }
		if($user->getPassword() != Factory::getHash($password)) { throw new \Exception('Signin failed', self::USER_SIGNIN_FAILURE); }
		$_SESSION['__session']['owner'] = array(
											'id' => $user->getId(),
											'email' => $user->getEmail(),
											'name' => $user->getName()
											);
		throw new \Exception($authToken, self::USER_SIGNIN_SUCCESS);
	}
	
	public static function static_signin($email, $password,$device=null) {
		$documentManager = Object::getDocumentManager();
		$user = $documentManager->getRepository('Weask\\User')->findOneBy(array('email' => $email));

		if(!$user instanceof User) { throw new \Exception('User does not exist', User::ERROR_USER_NOT_EXIST); }
		if($user->getPassword() != Factory::getHash($password)) { throw new \Exception('Signin failed', self::USER_SIGNIN_FAILURE); }
		if(!$device instanceof Device) { $device = new Device(); }
		$deviceSession = new DeviceSession();
		$authToken = Factory::getSecretToken();
		$deviceSession->setUser($user)
					->setLoggedTime(time())
					->setToken($authToken);
		self::removeDanglingSessions($documentManager, $user);
		self::persistDeviceSession($documentManager, $device, $deviceSession);
		Log::write('Successfully signed in user '.$user->getName());
		throw new \Exception($authToken, self::USER_SIGNIN_SUCCESS);
	}
	
	public static function static_signin_username($username, $password,$device=null) {
		$documentManager = Object::getDocumentManager();
		$user = $documentManager->getRepository('Weask\\User')->findOneBy(array('username' => $username));
		
		if(!$user instanceof User) { throw new \Exception('User does not exist', User::ERROR_USER_NOT_EXIST); }
		if($user->getPassword() != Factory::getHash($password)) { throw new \Exception('Signin failed', self::USER_SIGNIN_FAILURE); }
		if(!$device instanceof Device) { $device = new Device(); }
		$deviceSession = new DeviceSession();
		$authToken = Factory::getSecretToken();
		$deviceSession->setUser($user)
					->setLoggedTime(time())
					->setToken($authToken);
		self::removeDanglingSessions($documentManager, $user);
		self::persistDeviceSession($documentManager, $device, $deviceSession);
		Log::write('Successfully signed in user '.$user->getName());
		throw new \Exception($authToken, self::USER_SIGNIN_SUCCESS);
	} 
	
	public static function fb_signin($fb_id, $token, $device) {
		$documentManager = Object::getDocumentManager();
		$facebook_session = new FacebookSession($token);
		$request = new FacebookRequest($facebook_session, 'GET', '/me');
		$fb_user_profile = $request->execute()->getGraphObject(GraphUser::className());
		
		if($fb_id != $fb_user_profile->getId()) { throw new \Exception('Facebook authentication failed.'); }
		if(!$fb_user_profile instanceof GraphUser) { throw new \Exception('Not a valid user. Facebook authentication failed.'); }
		$user = $documentManager->getRepository('Weask\\User')->findOneBy(array('facebook_id' => $fb_user_profile->getId()));
		if(!$user instanceof User) { throw new \Exception('Use does not exist.', User::ERROR_USER_NOT_EXIST); }
		if(!$device instanceof Device) { $device = new Device(); }
		$deviceSession = new DeviceSession();
		$authToken = Factory::getSecretToken();
		$deviceSession->setUser($user)
						->setLoggedTime(time())
						->setToken($authToken);
		self::removeDanglingSessions($documentManager, $user);
		self::persistDeviceSession($documentManager, $device, $deviceSession);
		Log::write('Successfully signed in user '.$user->getName());
		throw new \Exception($authToken, self::USER_SIGNIN_SUCCESS);
	}
	
	public static function common_signin() {
		switch (func_get_arg(0)) {
			case self::SIGNIN_TYPE_EMAIL:
				self::static_signin(func_get_arg(1), func_get_arg(2), func_get_arg(3));
				break;
			case self::SIGNIN_TYPE_USERNAME:
				self::static_signin_username(func_get_arg(1), func_get_arg(2), func_get_arg(3));
				break;
			case self::SIGNIN_TYPE_FB:
				Log::write('Signin through Facebook ');
				self::fb_signin(func_get_arg(1), func_get_arg(2), func_get_arg(3));
				break;
			default:
				break;
		}
		throw new \Exception('Not a valid signin type');
	}
	
	public static function removeDanglingSessions($documentManager, $user) {
		
		$signedDevices = $documentManager->createQueryBuilder('Portus\\Auth\\Device\\Device')
								->field('activeSession.user.$id')
								->equals(new \MongoId($user->getId()))
								->getQuery()->execute();
		foreach ($signedDevices as $device) {
			Log::write('Removing '. $user->getName(). ' from '.$device->getDeviceId());
			$active_session = $device->getActiveSession();
			$device->removeActiveSession();
			$device->addToHistory($active_session);
			$documentManager->persist($device);
		}
		$documentManager->flush();
		return true;
	}
	
	public static function persistDeviceSession($documentManager, Device $device, $session) {
		try {
			$currentSession = $device->getActiveSession();
			
			if(!is_null($currentSession)) {
				$device->addToHistory($currentSession);
			}
			$device->setActiveSession($session);
			$documentManager->persist($device);
			$documentManager->flush();
		}
		catch (DeviceSessionException $e) {
			Log::write('Exception => '. $e->getMessage());
		}
		catch (\Exception $e) {
			Log::write('Exception => '. $e->getMessage());
		}
	}
}