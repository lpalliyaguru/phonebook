<?php
namespace Portus\Auth\Device;
use Portus\Log\Log;
use Portus\Db\Db;
use Portus\Auth\Device\DeviceSession;
use Portus\Auth\Device\DeviceSessionException;
use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use Doctrine\ODM\MongoDB\Mapping\Annotations\EmbedOne;
use Doctrine\ODM\MongoDB\Mapping\Annotations\EmbedMany;
use Portus\Factory\Object;
/**
 * 
 * @ODM\Document
 *
 */
class Device {
	const DEVICE_ID_KEY = 'Device-ID';
	
	const TYPE_IOS = 0;
	const TYPE_ANDROID = 1;
	const TYPE_WINDOWS = 2;
	/**
	 * 
	 * @ODM\Id
	 */
	private $id;
	/**
	 * 
	 * @ODM\String
	 */
	private $deviceId;
	/**
	 * 
	 * @ODM\EmbedOne(targetDocument="Portus\Auth\Device\DeviceSession")
	 */
	private $activeSession;
	/**
	 * 
	 * @ODM\EmbedMany
	 */
	private $history = array();
	/**
	 * 
	 * @ODM\String
	 */
	private $pn_token = null;
	/**
	 * 
	 * @ODM\Int
	 */
	private $type = null;
	/**
	 * 
	 * @ODM\Int
	 */
	private $notif_count = null;
	
	public function getId() {
		return $this->id;		
	}
	
	public function setId($id) {
		$this->id = $id;
		return $this;	
	}
	
	public function setDeviceId($id) {
		$this->deviceId = $id;
		return $this;
	}
	
	public function getDeviceId() {
		return $this->deviceId;
	}
	/**
	 * 
	 * @return Portus\Auth\Device\DeviceSession
	 */
	public function getActiveSession() {
		return $this->activeSession;
	}
	
	public function setActiveSession(DeviceSession $session) {
		$this->activeSession = $session;	
		return $this;
	}
	
	public function removeActiveSession() {
		$this->activeSession = null;
		return $this;
	}
	
	public function getHistory() {
		return $this->history;
	}
	
	public function addToHistory(DeviceSession $session) {
		$this->history[] = $session;
		return $this;
	}
	
	public function getPushNotificationToken() {
		return $this->pn_token;
	}
	
	public function setPushNotificationToken($token) {
		$this->pn_token = $token;
		return $this;
	}
	
	public function getNotificationCount() {
		return $this->notif_count;
	}
	
	public function setNotificationCount($count) {
		$this->notif_count = $count;
		return $this;
	}
	
	public function setDeviceType($type) {
		if(in_array($type, array(self::TYPE_IOS, self::TYPE_ANDROID, self::TYPE_WINDOWS))) {
			$this->type = $type;
			return $this;
		}
		throw new \Exception('Not a valid device type'); 
	}
	
	public function getDeviceType() {
		return $this->type;
	}
	
	public static function getUserSignedDevice($user) {
		$db = Object::getDocumentManager();
		$device = $db->createQueryBuilder('Portus\\Auth\\Device\\Device')
						->field('activeSession.user.$id')
						->equals(new \MongoId($user->getId()))
						->getQuery()
						->getSingleResult();
		return $device;
	}
}