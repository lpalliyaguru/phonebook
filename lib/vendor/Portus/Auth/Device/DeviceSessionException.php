<?php
namespace Portus\Auth\Device;
use Portus\Log\Log;
class DeviceSessionException extends \Exception {
	const TYPE_OK = 120;
	const TYPE_ERROR = 121;
	const TYPE_NOT_FOUND = 122; 
	
	public function __construct($message=null, $code=null, $previous=null) {
		Log::write(__METHOD__. ' : '.$message);
		parent::__construct($message,$code, $previous);
	}	
}

