<?php
namespace Portus\Auth\Device;

use Gt\User\AbstractUser;
use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use Doctrine\ODM\MongoDB\Mapping\Annotations\EmbeddedDocument;
/**
 * @EmbeddedDocument 
 *
 */
class DeviceSession {
	/**
	 * 
	 * @ODM\ReferenceOne(targetDocument="Weask\User", cascade="persist")
	 */
	private $user;
	/**
	 * 
	 * @ODM\Float
	 */
	private $logged_time;
	/**
	 * 
	 * @ODM\String
	 */
	private $token = null;
	
	public function getUser() {
		return $this->user;
	}
	
	public function setUser($user) {
		$this->user = $user;
		return $this;
	}
	
	public function getLoggedTime() {
		return $this->logged_time;
	}
	
	public function setLoggedTime($time) {
		$this->logged_time = $time;
		return $this;
	}
	
	public function getToken() {
		return $this->token;
	}
	
	public function setToken($token) {
		$this->token = $token;
		return $this;
	}
}