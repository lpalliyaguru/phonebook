<?php
namespace Portus\File;

class Mimes {
	
	private static $mimes = array('application/pdf' => 'pdf',
								'image/jpeg' => 'jpg',
								'text/plain' => 'txt',
								'text/csv' => 'csv',
								'image/bmp' => 'bmp',
								'image/png' => 'png',
								'audio/mpeg' => 'mp3',
								'audio/ogg' => 'ogg',
								'audio/mp4' => 'mp4',
								'audio/mp3' => 'mp3',
								'audio/iLBC' => 'lbc',
								
							);
	
	public static function getMimes() {
		return self::$mimes;
	}
	
	public static function getFileExtensionByMime($mime) {
		if(array_key_exists($mime, self::$mimes)) {
			return self::$mimes[$mime];
		}
		return null;
	}
	
}