<?php
namespace Portus\Db;

use Doctrine\ODM\MongoDB\Configuration as Config;
use Doctrine\Common\Collections\Selectable;
use Doctrine\ODM\MongoDB\Mapping\Driver\AnnotationDriver;

class Configuration {
	private static $instance = null;
	
	public static function getInstance() {
		if(!self::$instance instanceof Config) {
			self::$instance = new Config();
		}
		self::$instance->setProxyDir(__DIR__.'/Proxies');
		self::$instance->setProxyNamespace('Proxies');
		self::$instance->setHydratorDir(__DIR__.'/Hydrators');
		self::$instance->setHydratorNamespace('Hydrators');
		self::$instance->setDefaultDB('gt');
		self::$instance->setMetadataDriverImpl(AnnotationDriver::create(APP_ROOT.'/lib/vendor/Gt/'));
		return self::$instance;
	}
}