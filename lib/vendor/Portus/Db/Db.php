<?php
namespace Portus\Db;

class Db {
	private $_con;
	private $_result;
	private $_error;
	public $numResults;
	private static $_instance;
	private static $_mongo_instance;
	
	public function __construct() {
		$this->connect();	
	}
	
	public static function getInstance() {
		if(!self::$_instance instanceof Db) {
			self::$_instance = new Db();
		}
		return self::$_instance;
	} 
	
	public static function getMongoInstance() {
		if(!self::$_mongo_instance instanceof \MongoClient) {
			self::$_mongo_instance = new \MongoClient( 'mongodb://'.DB_MONGO_HOST.':'.DB_MONGO_PORT);
		}
		$db = self::$_mongo_instance->selectDB(DB_MONGO_NAME);		
		return $db;
	}
	
	public function connect() {
		if(!$this->_con) {
			$this->_con = mysql_connect(DB_HOST,DB_USER,DB_PASSWORD);
		
			if(!$this->_con) { return false; }
			$select_db = mysql_select_db(DB_NAME,$this->_con);
		
			if(!$select_db) { return false; }
			return true;
		}
		else {
			return false;
		}
	}
	
	public function query($q) {
		$result = @mysql_query($q,$this->_con);
		$this->_result = null;
		if($result) {
			$this->numResults = mysql_affected_rows();
			for($i = 0; $i < $this->numResults; $i++) {
				$r = mysql_fetch_array($result);
				$key = array_keys($r);
				for($x = 0; $x < count($key); $x++){
					if(!is_int($key[$x])){
						if(mysql_num_rows($result) >= 1)
							$this->_result[$i][$key[$x]] = $r[$key[$x]];
						else if(mysql_num_rows($result) < 1)
							$this->_result = null;
						else
							$this->_result[$key[$x]] = $r[$key[$x]];
					}
				}
			}
			return true;
		}
		else
		{
			$this->_error = mysql_error($this->_con);
			return false;
		}
		
	}
	
	public function getResult() {
		return $this->_result;
	}
	
	public function getError() {
		return $this->_error;
	}
	
	
	
}
?>