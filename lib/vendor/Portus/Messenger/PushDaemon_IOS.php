<?php
namespace Portus\Messenger;
use ApnsPHP\ApnsPHP_Push;
use ApnsPHP;
use Portus\Log\Log;

class PushDaemon_IOS {
	
	private $instance = null;
	private $loggableResult = null;
	
	public function __construct() {
		$this->instance = new ApnsPHP_Push(ApnsPHP\ApnsPHP_Abstract::ENVIRONMENT_SANDBOX, 
					APP_ROOT.'/cert/server_certificates_bundle_sandbox.pem');
		$this->instance->setRootCertificationAuthority(APP_ROOT.'/cert/entrust_root_certification_authority.pem');
		$this->instance->connect();	
	}
	
	public function addMessage($message) {
		Log::write(__METHOD__.' Adding message');
		$this->instance->add($message);
	}
	
	public function sendAll() {
		try {
			ob_start();
			$this->instance->send();
			$this->loggableResult = ob_get_contents();
			ob_end_clean();
			$this->logResult();
		}
		catch (\Exception $e) {
			Log::write(__METHOD__.' '.$e->getMessage());
		}
	}
	
	public function logResult() {
		$aErrorQueue = $this->instance->getErrors();
		Log::write('Push Notification Result => ' . json_encode($this->loggableResult));
		Log::write('Push Notification Error => ' . json_encode($aErrorQueue));		
	}
	
	public function __destruct() {
		$this->instance->disconnect();
	}
}