<?php
namespace Portus\Helper;

use Portus\Log\Log;
class Paginator {
	
	private $position = 0;
	private $pageSize = 10;
	private $reference;
	private $nextPageToken = null;
	private $queryBuilder = null;
	private $params = array();
	
	public $hasNextPage = false;
	
	public function getPageSize() {
		return $this->pageSize;
	}	
	
	public function setPageSize($size) {
		$this->pageSize = $size;
		return $this;
	}
	
	public function setPosition($position) {
		$this->position = $position;
		return $this;
	}
	
	public function getPosition() {
		return $this->position;
	}
	
	public function setReference($ref) {
		$this->reference = $ref;
		return $this;
	}
	
	public function getReference() {
		return $this->reference;
	}
	
	public function setNextPageToken($token) {
		$this->nextPageTokens = $token;
		return $this;
	}
	
	public function getNextPageToken() {
		return $this->nextPageToken;
	}
	
	public function setQueryBuilder($qb) {
		$this->queryBuilder = $qb;
		return $this;
	}
	
	public function getResultSet() {
		$list = $this->queryBuilder->skip($this->position)
							->limit($this->pageSize)
							->getQuery()
							->execute();
		$totalDocuments = $this->queryBuilder->getQuery()->count();
		Log::write(__METHOD__.' '.$totalDocuments.' documents');
		$this->hasNextPage = $totalDocuments > ($this->position + $this->pageSize);
		Log::write($this->position + $this->pageSize);
		Log::write('Has next page ' . json_encode($this->hasNextPage));
		return $list;
	}
	
	public function addParam($key, $value) {
		$this->params[$key] = $value;
		return $this;
	}
	
	public function buildNextPageToken() {
		$serializable = array(
				'reference' => $this->reference,
				'position' => $this->position + $this->pageSize,
				'page_size' => $this->pageSize,
				'params' => $this->params
		);
		
		return Cipher::encrypt(json_encode($serializable));
	}
	
	public static function extractNextPageToken($token) {
		$object = @json_decode(Cipher::decrypt($token));
		if($object) {
			return $object;
		}
		throw new \Exception('Not a valid next page token');
		
	}
}