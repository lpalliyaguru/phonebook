<?php
namespace Portus;

use Portus\Db\Configuration;
use Portus\Auth\Auth;
use Portus\Auth\Device\Device;
use Doctrine\MongoDB\Connection;
use Doctrine\ODM\MongoDB\DocumentManager;
use Doctrine\ODM\MongoDB\Mapping\Driver\AnnotationDriver;
use Portus\Factory\Object;
use Portus\Log\Log;
use Portus\Request\Request;
use Portus\Locale\LocaleInstance;
use Portus\Locale\LocaleHandler;

class Controller {
	/**
	 * 
	 * @var $_request Portus\Request\Request
	 */
	protected $_request = null;
	protected $_sessionOwner = null;
	protected $_isApiController = false;
	
	public function __construct($isApiController=false) {
		$this->_request = new Request($isApiController);
		
		if($isApiController) {
			//Log::write('getting session owner for api');
			$this->_sessionOwner = Auth::getApiSessionOwner($this->getDevice(), $this->getRequestAuthenticationToken());
		}
		else {
			$this->_sessionOwner = Auth::getWebSessionOwner();
		}
		$acceptLanguage = !is_null($lang = $this->_request->getAcceptLanguage()) ? $lang : 'en_GB';
		
		$localeHandler = new LocaleHandler(new LocaleInstance($acceptLanguage));
		$localeHandler->setLocale();
		Log::write('Portus is accepting language => '.$localeHandler->getLocale());
	}
	
	public function setRequest($request) {
		$this->_request = $request;
	}
	
	public function getRequest() {
		return $this->_request;
	}
	
	public function getParam($key) {
		return $this->_request->getParam($key);
	}
	
	public function hasParam($key) {
		return $this->_request->hasParam($key);
	}
	
	public function getFiles() {
		if((bool)func_num_args()) {
			return $this->_request->getFiles(func_get_arg(0));
		}
		return $this->_request->getFiles();
	}
	
	protected function getRequestAuthenticationToken() {
		return $this->_request->getHeaders(Auth::AUTH_KEY_NAME);
	}
	
	protected function getHeaders() {
		$this->_request->getHeaders();
	}
	/**
	 * @return Device
	 */
	protected function getDevice() {
		return $this->_request->getDevice();
	}
	/**
	 * @return DocumentManager
	 */
	protected function getDocumentManager() {
		return Object::getDocumentManager();
	}
	
	public function isApiController() {
		return $this->_isApiController;
	}
}

?>