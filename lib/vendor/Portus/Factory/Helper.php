<?php
namespace Portus\Factory;

class Helper {
	
	public static function formatLatlong($lat, $long) {
		return array('lat' => $lat, 'lng' => $long);
	}
	
	public static function pluralize($count, $singular, $plural) {
		return $count > 1 ? $plural : $singular;
	}
	/**
	 * 
	 * @param DateTime $datetime
	 * @return string
	 */
	public static function time_offset($datetime){
		$o = (int)(time() - $datetime->getTimestamp());
		switch(true) {
			case ($o <= 5): return 'just now'; break;
			case ($o < 20): return $o . ' seconds ago'; break;
			case ($o < 40): return 'half a minute ago'; break;
			case ($o < 60): return 'less than a minute ago'; break;
			case ($o <= 90): return '1 minute ago'; break;
			case ($o <= 59*60): return round($o / 60) . ' minutes ago'; break;
			case ($o <= 60*60*24): return round($o / 60 / 60) . ' hours ago'; break;
			case ($o <= 60*60*24*1.5): return 'yesterday'; break;
			case ($o <= 60*60*24*6.5): return date('l \a\t h:i A',$t); break;
			case ($o <= 60*60*24*28.5): return date('h:i A M jS',$t); break;
			//case($o < 60*60*24*7): return round($o / 60 / 60 / 24) . " days ago"; break;
			//case($o <= 60*60*24*9): return "1 week ago"; break;
			default: return date('h:i A M jS Y', $t);
		}
	}
}