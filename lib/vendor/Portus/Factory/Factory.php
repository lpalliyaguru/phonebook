<?php
namespace Portus\Factory;
class Factory {
	
	public static function getHash($string) {
		return md5($string);
	}
	
	public static function getSecretToken() {
		$token = md5(rand(9999,99999)).md5(microtime(true));
		return $token;
	}
	
	public static function getStaticPassword() {
		$charlist = md5(microtime(true));
		return strtoupper(substr($charlist, 10,8));
	}
}
?>