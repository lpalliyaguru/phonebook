<?php
namespace Portus\Factory;
use Doctrine\MongoDB\Connection;
use Doctrine\ODM\MongoDB\Configuration;
use Doctrine\ODM\MongoDB\DocumentManager;
use Doctrine\ODM\MongoDB\Mapping\Driver\AnnotationDriver;
use Portus\Log\Log;

class Object {

	private static $instance = null;
	
	public static function getDocumentManager() {
		
		if(!self::$instance instanceof DocumentManager) {
			AnnotationDriver::registerAnnotationClasses();
			$connection = new Connection();
			$config = new Configuration();
			$config->setProxyDir(APP_ROOT.'/lib/vendor/Portus/Proxies');
			$config->setProxyNamespace('Proxies');
			$config->setHydratorDir(APP_ROOT.'/lib/vendor/Portus/Hydrators');
			$config->setHydratorNamespace('Hydrators');
			$config->setDefaultDB(APP_MONGO_DB_NAME);
			$config->setMetadataDriverImpl(AnnotationDriver::create(APP_ROOT.'/lib/vendor/Gt/'));
			self::$instance = DocumentManager::create($connection, $config);
		}
		return self::$instance;	
	}
}