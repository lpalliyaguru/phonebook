<?php
namespace Portus\Request;

use Portus\Auth\Device\Device;
use Portus\Auth\Device\DeviceSessionException;
use Portus\Factory\Object;
use Portus\Log\Log;
class Request {
	
	const HEADER_ACCEPT_LANGUAGE = 'Accept-Language';
	private $_get = null;
	private $_post = null;
	private $_files = null;
	private $_is_ajax = null;
	private $_uri = null;
	private $_router_path = null;
	private $_headers = array();
	private $_device= null;
	private $_userAgent = null;
	private $_isApiRequest = false;
	
	public function __construct($isApiRequest=false) {
		$this->_isApiRequest = $isApiRequest;
		$this->bake();
		if($isApiRequest) {
			$this->_device = $this->getDevice();	
		}
	}
	
	public function bake() {
		$this->_get = (object)array_map(array($this,"cleanMore"), $_GET);
		$this->_post = (object)array_map(array($this,"cleanMore"), $_POST);
		$this->_files = (object)$_FILES;
		$this->_is_ajax = (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && $_SERVER['HTTP_X_REQUESTED_WITH'] == "XMLHttpRequest") ? true : false;
		$this->_uri = $_SERVER['REQUEST_URI'];
		$this->_router_path = isset($_GET['url']) ? $_GET['url'] : null;
		$this->_headers = getallheaders();
		$this->_userAgent = isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : null;
	}
	
	public  function hasParam($key) {
		return (property_exists($this->_get, $key) || property_exists($this->_post,$key)) ? true : false;
	}
	
	public  function getParam($key) {
		if(property_exists($this->_get, $key)) { return $this->_get->$key; }
		else if(property_exists($this->_post,$key)) { return $this->_post->$key; }
		else { return false; }
	}

	public function getRouterPath() {
		return $this->_router_path;
	}
	
	public function getURI() {
		return $this->_uri;
	}
	
	public function getFiles() {
		if((bool)func_num_args()) {
			if(property_exists($this->_files, func_get_arg(0))) {
				$file = func_get_arg(0);
				return $this->_files->$file;
			}
			else {
				if(func_num_args() == 2 && func_get_arg(1) === true) {
					throw new \Exception('Cannot find file '.func_get_arg(0));
				}
				else {
					return null;
				}
			}
		}
		return $this->_files;
	}
	
	public function isAjax() {
		return $this->_is_ajax;
	}
	
	public function cleanMore($value){
		$value = is_array($value) ? array_map(array($this,'cleanMore'),$value) : $this->clean($value);
		return $value;
	}
	
	public function clean($value) {
		return $value;
	}
	
	public function getHeaders() {
		if((bool)func_num_args()) {
			return isset($this->_headers[func_get_arg(0)]) ? $this->_headers[func_get_arg(0)] : null;
		}
		return $this->_headers;
	}
	
	public function getAcceptLanguage() {
		return $this->getHeaders(self::HEADER_ACCEPT_LANGUAGE);
	}

	public function getDevice() {
		$deviceId = isset($this->_headers[Device::DEVICE_ID_KEY]) ? $this->_headers[Device::DEVICE_ID_KEY] : null;

		if($this->_isApiRequest && is_null($deviceId) ) { throw new DeviceSessionException('Unable to find the device', DeviceSessionException::TYPE_NOT_FOUND); }
		$documentManager = Object::getDocumentManager();
		$device = $documentManager->getRepository('Portus\\Auth\\Device\\Device')->findOneBy(array('deviceId' => $deviceId));
		
		if($device instanceof Device) {
			return $device;
		}
		else {
			$device = new Device();
			$device->setDeviceId($this->_headers[Device::DEVICE_ID_KEY]);
			return $device;
		}
	}
	
	public function getUserAgent() {
		return $this->_userAgent;
	}
}
?>