<?php
namespace Portus\Locale;
use Portus\Locale\LocaleInstance;
use Portus\Log\Log;

class LocaleHandler {
	
	public static $accepted_lang_types = array(
			'en_GB' => 'English', 
			'en_US' => 'English',
			'zh_CN' => 'Chinese', 
			'ko_KR' => 'Korean', 
			'id_ID' => 'Indonesian', 
			'ja_JP' => 'Japanese'
	);
	
	private $localeInstance; 
	
	public function __construct(LocaleInstance $localeInstance) {
		$this->localeInstance = $localeInstance;
	}
	
	public function setLocale() {
		/* putenv('LC_ALL='.$this->localeInstance->getLocale());
		setlocale(LC_ALL, $this->localeInstance->getLocale());
		
		Log::write('locale path ' . APP_LOCALE_PATH);
		bindtextdomain('messages', APP_LOCALE_PATH);
		textdomain('messages'); */
		return true;
	}
	
	public function getLocale() {
		return $this->localeInstance->getLocale();
	}
}