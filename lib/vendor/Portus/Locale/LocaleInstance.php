<?php
namespace Portus\Locale;
use Portus\Locale\LocaleHandler;
use Portus\Log\Log;
class LocaleInstance {
	private $locale;
	
	public function __construct($locale) {
		if(!array_key_exists($locale, LocaleHandler::$accepted_lang_types)) {
			$this->locale = 'en_GB';
		}
		else {
			$this->locale = $locale;
		}
	}
	
	public function getLocale() {
		return $this->locale;
	}
}