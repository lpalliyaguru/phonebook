<?php
namespace Jabber;
use Portus\Log\Log;

class AddMessenger {

	function __construct(&$jab,$name,$pass) {
		Log::write(__METHOD__.' '.$pass);
		$this->jab = &$jab;
		$this->jab->NewUserName = $name;
		$this->jab->NewUserPass = $pass;
	}

	function handleConnected() {
		global $AddUserErrorCode;
		$AddUserErrorCode = 12002;
		// now that we're connected, tell the Jabber class to login
		Log::write('Trying to log '.JABBER_SERVER.' with user '.JABBER_USERNAME.' '.JABBER_PASSWORD);
		$this->jab->login(JABBER_USERNAME,JABBER_PASSWORD);

	}

	// called after a login to indicate the the login was successful
	function handleAuthenticated() {
		global $AddUserErrorCode;
		$AddUserErrorCode = 12003;
		$this->jab->adduser_init();
	}
}
