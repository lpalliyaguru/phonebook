<?php
class AddVcard {

	function AddVcard(&$jab,$name,$pass,$firstn,$lastn,$patro,$sex,$role) {
		$this->jab = &$jab;
		$this->jab->NewUserName = $name;
		$this->jab->NewUserPass = $pass;
		$this->GivenName = iconv('CP1251','UTF-8',$firstn); // conversion from russian charset :)
		$this->FamilyName = iconv('CP1251','UTF-8',$lastn);
		$this->MiddleName = iconv('CP1251','UTF-8',$patro);
	}

	function handleConnected() {
		global $AddVcardErrorCode;
		$AddVcardErrorCode = 14002;
		$this->jab->login($this->jab->NewUserName,$this->jab->NewUserPass);
	}

	function handleAuthenticated() {
		global $AddVcardErrorCode;
		$AddVcardErrorCode = 14003;
		$this->jab->addvcard_request($this->GivenName, $this->FamilyName, $this->MiddleName, $this->UserRole);
	}
}
