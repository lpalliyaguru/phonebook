<?php
namespace Phonebook;
use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
/**
 * @ODM\Document
 */
class Entry {
	/**
	 * 
	 * @ODM\Id
	 */
	private $id;
	/**
	 * 
	 * @ODM\String
	 */
	private $name;
	/**
	 * 
	 * @ODM\String
	 */
	private $phone;
	/**
	 * 
	 * @ODM\Date
	 */
	private $addedOn;
	
	private $avatar;
	
	public function getId() {
		return $this->id;
	}
	
	public function setId($id) {
		$this->id = $id;
		return $this;
	}
	
	public function getName() {
		return $this->name;
	}
	
	public function setName($name) {
		$this->name = $name;
		return $this;
	}
	
	public function getPhone() {
		return $this->phone;		
	}
	
	public function setPhone($phone) {
		$this->phone = $phone;
		return $this;
	}
	
	public function getAddedDate() {
		return $this->addedOn;
	}
	
	public function setAddedDate($date) {
		$this->addedOn = $date;
		return $this;
	}
}
