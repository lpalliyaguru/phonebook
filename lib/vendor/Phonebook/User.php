<?php
namespace Weask;
use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
/**
 * @ODM\Document
 * @ODM\Index(keys={"latlng"="2dsphere"})
 */
class User {
	const ERROR_USER_EXIST = 101;
	const ERROR_USER_NOT_EXIST = 102;
	const TYPE_INDIVIDUAL = 0;
	const TYPE_BUSINESS = 1;
	
	/**
	 * @ODM\Id
	 */
	private $id;
	/**
	 * @ODM\Int
	 */
	private $type;
	/**
	 * @ODM\String
	 */
	private $name;
	/**
	 * @ODM\String
	 */
	private $address;
	/**
	 * 
	 * @ODM\String
	 */
	private $website;
	/**
	 * @ODM\Collection
	 */
	private $latlng;
	/**
	 * 
	 * @ODM\String
	 */
	private $email;
	/**
	 * 
	 * @ODM\String
	 */
	private $username;
	/**
	 *
	 * @ODM\String
	 */
	private $password;
	/**
	 * 
	 * @ODM\ReferenceMany(targetDocument="Weask\User", cascade="persist")
	 */	
	private $followings = array();
	
	/**
	 * @ODM\String
	 */
	private $facebook_token;
	/**
	 * 
	 * @ODM\String
	 */
	private $facebook_id;
	/**
	 * @ODM\String
	 */
	private $language;
	/**
	 * @ODM\String
	 */
	private $picture;
	/**
	 * @ODM\String
	 */
	private $phone;
	
	public function getId() {
		return $this->id;
	}
	
	public function setId($id) {
		$this->id = $id;
		return $this;
	}
	
	public function getType() {
		return $this->type;
	}
	
	public function setType($type) {
		$this->type = $type;
		return $this;
	}
	
	public function getName() {
		return $this->name;
	}
	
	public function setName($name) {
		$this->name = $name;
		return $this;
	}
	
	public function getAddress() {
		return $this->address;
	}
	
	public function setAddress($address) {
		$this->address = $address;
		return $this;
	}
	
	public function getWebsite() {
		return $this->website;
	}
	
	public function setWebsite($website) {
		$this->website = $website;
		return $this;
	}
	
	public function setLatlong($lat, $long) {
		$this->latlng = array(floatval($long), floatval($lat));
		return $this;
	}
	
	public function getLatLong() {
		return $this->latlng;
	}
	
	public function getEmail() {
		return $this->email;
	}
	
	public function setEmail($email) {
		$this->email = $email;
		return $this;
	}

	public function getUserName() {
		return $this->username;
	}
	
	public function setUserName($username) {
		$this->username = $username;
		return $this;
	}
	
	public function getPassword() {
		return $this->password;
	}
	
	public function setPassword($password) {
		$this->password = $password;
		return $this;
	}
	
	public function getFacebookToken() {
		return $this->facebook_token;
	}
	
	public function setFacebookToken($token) {
		$this->facebook_token = $token;
		return $this;
	}
	
	public function getFacebookId() {
		return $this->facebook_id;
	}
	
	public function setFacebookId($id) {
		$this->facebook_id = $id;
		return $this;
	}
	
	public function getPhone() {
		return $this->phone;
	}
	
	public function setPhone($phone) {
		$this->phone = $phone;
		return $this;
	}
	
	public function getLanguage() {
		return $this->language;
	}
	
	public function setLanguage($lang) {
		$this->language = $lang;
		return $this;
	}
	
	public function getPicture() {
		return $this->picture;
	}
	
	public function getPictureUrl() {
		return !is_null($this->picture) ? APP_CDN_PATH . '/pp/'.$this->picture : null;
	}
	
	public function setPicture($picture) {
		$this->picture = $picture;
		return $this;
	}
	
	public function addFollowing($user) {
		if(!$this->isAFollowingUser($user)) {
			$this->followings[] = $user;
		}
		return $this;
	}
	
	public function removeFollowing($user) {
		$followings = array();
		foreach ($this->followings as $following) {
			if($following->getId() != $user->getId()) {
				$followings[] = $following;
			}
		}
		$this->followings = $followings;
	}
	
	public function isAFollowingUser($user) {
		foreach ($this->followings as $following) {
			if($following->getId() == $user->getId()) {
				return true;
			}
		}
		return false;
	}
	
	public function getFollowings() {
		return $this->followings;
	}
	
	public function getExposable() {
		return array(
				'name' => $this->getName(),
				'address' => $this->getAddress(),
				'username' => $this->getUserName(),
				'id' => $this->getId(),
				'email' => $this->getEmail(),
				'picture' => $this->getPictureUrl(),
				'facebook_id' => $this->getFacebookId(),
				'phone' => $this->getPhone(),
				'counts' => array('followings' => count($this->followings))
		);
	}
}