<script type="text/javascript">
	$(function(){
		$('#contact-import-form').validate({
			rules:{file:{required:true}},
			submitHandler:function(form) {
				$(form).ajaxSubmit({
					beforeSubmit:function(){
						$(form).find(':submit').button('loading');
						},
					success:function(data){
						$(form).find(':submit').button('reset');
						Helper.closeModal();
						if(data.success) {
							$('#entry-list').slideUp().find('tbody').html(data.list_html);
							$('#entry-list').slideDown();
						}
						else {
							alert(data.message);
							}
					}
					});
			}
		});

		});
</script>

<div class="modal-dialog">
	<div class="modal-content">
    	<form action="/phonebook/import" method="post" id="contact-import-form">
	    	<div class="modal-header">
	        	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	        	<h4 class="modal-title" >Import contacts</h4>
	      	</div>
	      	<div class="modal-body">
      		 	
				<div class="row">
					<div class="col-md-12">
						<label>Choose the contact file. (Accepted formats : csv, tsv)</label>
						<div class="input-group col-md-6">
      		 				<input type="file" class="form-control" name="file"/>
	      		 		</div>
					</div>
				</div>
	      	</div>
	      	<div class="modal-footer">
	      		<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	        	<input type="submit" class="btn btn-primary" value="Import" data-loading-text="Importing">
	      	</div>
		</form>
    </div>
</div>