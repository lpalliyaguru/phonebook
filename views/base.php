<!DOCTYPE HTML>
<html>
<?php include_once APP_TEMPLATE_PATH.'/layout/head.php'; ?>
<body>
		<?php
		
		include_once APP_TEMPLATE_PATH.'/layout/header.php';
		foreach ($__page->sections as $page_section) {
			include_once $page_section;
		}
		APP_TEMPLATE_PATH.'/layout/footer.php'; 
		?>
	
		<div id="modal-window" class="modal fade"></div>
</body>
</html>