<div class="container wrapper">
	<div class="row">
		<div class="col-md-6">
			<form class="navbar-form pull-left form-search" id="search-form" action="/phonebook/search">
				<div class="input-group col-md-6">
					<input type="text" class="form-control" name="term" placeholder="Search">
					<span class="input-group-addon"><i class="fa fa-search"></i></span>
				</div>
	  		</form>
		</div>
		<div class="col-md-6">
			<div class="btn-group pull-right">
				<a href="/phonebook/show_import_box" type="button" class="btn btn-default system-dialog"><i class="fa fa-upload"></i> Import</a>
				<a href="/phonebook/export" class="btn btn-default"><i class="fa fa-download"></i> Export</a>
				<button type="button" class="btn btn-default add-contact"><i class="fa fa-plus"></i> Add Entry</button>
			</div>
		</div>
	</div>
	<div class="row">
		<table class="table table-striped" id="entry-list">
	      	<thead>
		        <tr>
		          <th>Name</th>
		          <th>Phone</th>
		        </tr>
	      	</thead>
	      	<tbody>
	      		<?php include_once APP_TEMPLATE_PATH.'/section_phonebook.entry.list.php'; ?>
	     	</tbody>
	    </table>
	</div>
</div>