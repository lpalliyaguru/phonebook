<?php
foreach ($list as $entry) { ?>
<tr data-entry-id="<?php echo $entry->getId(); ?>">
    <td><p contenteditable="true" class="changeable" data-elem-name="name"><?php echo $entry->getName(); ?></p></td>
    <td><p contenteditable="true" class="changeable" data-elem-name="phone"><?php echo $entry->getPhone(); ?></p> </td>
    <td>
    	<button class="btn btn-danger pull-right btn-xs remove-entry" data-id="<?php echo $entry->getId(); ?>"><i class="fa fa-trash-o"></i></button>
    	<small><i class="fa fa-clock-o"></i> added on <?php echo Portus\Factory\Helper::time_offset($entry->getAddedDate()); ?></small>
    </td>
</tr>
<?php } ?>