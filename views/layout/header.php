<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Phonebook Entries</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Phonebook Entries</a>
        </div>
        <div class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
            <li><a href="#about">About</a></li>
          </ul>
          <div class="nav" id="waiting-div"><img src="<?php echo APP_CDN_PATH.'/images/waiting.gif'; ?>"></div>
        </div><!--/.nav-collapse -->
      </div>
</div>