<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?php echo $__page->title; ?></title>
<link rel="icon" href="<?php echo APP_CDN_PATH.'/images/favicon.ico'; ?>"/>
<link href="<?php echo APP_CDN_PATH; ?>/css/bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="<?php echo APP_CDN_PATH; ?>/css/font-awesome.css" rel="stylesheet" type="text/css">
<link href="<?php echo APP_CDN_PATH; ?>/css/theme.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<?php echo APP_CDN_PATH.'/js/jquery-1.11.1.min.js'?>"></script>
<script type="text/javascript" src="<?php echo APP_CDN_PATH.'/js/bootstrap.min.js'?>"></script>
<script type="text/javascript" src="<?php echo APP_CDN_PATH.'/js/script.js'?>"></script>

<?php foreach($__page->assets as $asset) { 
		@list($type, $asset_section,$place) = $asset;
		if($place == 'HEAD') { 
			if($type == 'STYLE') {
				echo '<link rel="stylesheet" type="text/css" href="'.$asset_section.'">';
			
			}
			else if($type == 'SCRIPT') {
				echo '<script type="text/javascript" src="'.$asset_section.'" ></script>';
			}
		}
	}
?>
</head>