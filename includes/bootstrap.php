<?php
use Portus\Request\Request;
use Portus\Auth\Device\DeviceSessionException;
use Portus\Log\Log;
use Doctrine\Common\Annotations\FileCacheReader;
use Doctrine\Common\Annotations\AnnotationReader;

class App {
	const APP_ENVIRONMENT_CLI = 'CLI';
	const APP_ENVIRONMENT_WEB = 'WEB';
	/**
	 * Portus Initialization 
	 */
	public static function init() {
		spl_autoload_register(array('Autoload','load'), true, true);
		if(APP_ENVIRONMENT == self::APP_ENVIRONMENT_CLI) {
			Log::write('Initializing the cli environment');
			self::init_cli();
		}
		else if(APP_ENVIRONMENT == self::APP_ENVIRONMENT_WEB) {
			Log::write('Initializing the web environment');
			self::init_web();
		}
	}	
	/**
	 * Portus cli Initialization
	 */
	public static function init_cli() {
		
	}
	/**
	 * Portus web Initialization 
	 */	
	public static function init_web() {
		try {
			$path = isset($_GET['url'])  && strlen($_GET['url']) != 0 ? $_GET['url'] : '/';
			$controllers = App::loadController($path);
			$ctrl = trim($controllers[0],'/');
			/* making namespace*/
			$ctrl = str_replace('/', '\\', $ctrl);
			if(!class_exists($ctrl)) { throw new Exception('Class '.$ctrl.' cannot be found.',404); }
			
			$action = $controllers[1];
			$controller = new $ctrl;
			if(!is_callable(array($controller,$action))) { throw new Exception('Unable to call method.',404); }
			/* Logging the Request */
			if($controller->isApiController()) {
				Log::write('Initializing Portus for '.$controller->getRequest()->getUserAgent(). '. Device '.$controller->getRequest()->getDevice()->getDeviceId());
			}
			else {
				Log::write('Initializing Portus for '.$controller->getRequest()->getUserAgent());
			}
			$controller->$action();
		}
		catch (DeviceSessionException $e) {
			switch ($e->getCode()) {
				case DeviceSessionException::TYPE_NOT_FOUND:
					echo json_encode(array('success' => false, 'message' => 'Unable to find device.'));
					break;
				default:
					echo json_encode(array('success' => false, 'message' => $e->getMessage()));
					break;
			}
		}
		catch (Exception $e) {
			Log::write($e->getMessage());
			switch ($e->getCode()) {
				case 404:
					header('HTTP/1.1 404 Not Found');
					header('Content-type:application/json');
					echo json_encode(array('success' => false, 'message' => 'Requested resource cannot be found'));
					break;
				default:
					header('Content-type:application/json');
					echo json_encode(array('success' => false, 'message' => $e->getMessage()));
					break;
			}
		}
	}
	/**
	 * Loading the controller
	 * @param String $path
	 * @throws Exception
	 * @return multitype:string |unknown
	 */
	public function loadController($path) {
		$app_controller_path = APP_ROOT.'/Controllers';
		if($path == '/') { return array('DefaultController', 'index'); }
		$route = self::loadRouteByLocator($path);
		if(!is_null($route)) {
			return $route;
		}
		
		$components = explode('/', trim($path,'/'));
		$method = 'index';
		$prependableControllerComponents = array();
		foreach($components as $position => $component) {
			$prependablePath = implode('/', $prependableControllerComponents);
			$directory = trim($prependablePath . '/' . ucfirst($component),'/');
			$assumedController = trim($prependablePath . '/' . self::getControllerMethodName($component),'/');
			
			if(file_exists($app_controller_path . '/' . $assumedController.'.php')) {
				if(isset($components[$position+1])) {
					$method = $components[$position+1];
				}
				return array($assumedController,$method);
			}
			else if(is_dir($app_controller_path.'/'.$directory) && !isset($components[$position+1])) {
				$indexController = $directory.'/IndexController.php';
				
				if(file_exists($app_controller_path . '/' . $indexController)) {
	  				return array($directory.'/IndexController','index');
				}
			}
			$prependableControllerComponents[] = ucfirst($component);
		}
		
		throw new Exception('Path '.$path.' cannot be found.',404);
	}
	/**
	 * Loading the controller by route for dynamic paths.
	 * @param String $path
	 * @return multitype:string |NULL
	 */
	public static function loadRouteByLocator($path) {
		$routes = array(
				'/^[0-9a-z-_A-Z]{2,80}$/' => array('PageController', 'index'),
				'/api\/v1\/user\/[0-9a-f]{1,24}\/followers/' => array('Api/V1/UserController', 'get_user_followers'),
				'/api\/v1\/user\/[0-9a-f]{1,24}\/following_people/' => array('Api/V1/UserController', 'get_user_followings')
		);
		foreach ($routes as $routePattern => $ctrl) {
			if(preg_match($routePattern, $path)) {
				return $ctrl;
			}
		}
		return null;
	}
	
	/**
	 * This method returns the controller method name
	 * @param String $pathComponent
	 * @return string
	 */
	private static function getControllerMethodName($pathComponent) {
		return ucfirst($pathComponent).'Controller';
	}
}
?>