<?php
class Autoload {

	public static function load($className) {
		$classFile = strtolower($className).'.class.php';

		if(file_exists(APP_ROOT.'/lib/'.$classFile)) {
			require_once APP_ROOT.'/lib/'.$classFile;
			return true;
		}

		$controller = $className.'.php';

		if(preg_match("/\\//", $className) !== false) {
			
			if(preg_match("/(?<=[a-z0-9])controller/i", $className) != false) {
				$className = 'Controllers/'.$className;
				$classFile = APP_ROOT.'/';
			}
			else {
				$classFile = APP_ROOT.'/lib/vendor/';
			}
			$file = str_replace('\\', '/', $className);
			$file = $file.'.php';
			$classFile .= $file;
			if(file_exists($classFile)) {
				require_once $classFile;
				return true;
			}
		}
		return false;
	}
}
