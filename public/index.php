<?php 
session_start();
date_default_timezone_set('Asia/Kuala_Lumpur');
define('APP_ENVIRONMENT', PHP_SAPI == 'cli' ? 'CLI' : 'WEB' );
define('__ROUTER_PATH', null);
require __DIR__.'/../config/config.php';
require __DIR__.'/../includes/autoload.php';
require __DIR__.'/../includes/bootstrap.php';
/* Initialize the application */
App::init();
?>
