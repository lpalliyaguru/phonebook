/**
 * 
 */

$(function(){
	$(document).on('click','.system-dialog', function(e){
		e.preventDefault();
		clickedElement=$(this);
		Helper.dialog($(this));
	})
	$(document).on('focusout','.changeable',function() {
		var tr = $(this).closest('tr');
			var dataSet = {
					id:tr.data('entry-id'),
					name:tr.find('[data-elem-name="name"]').text(),
					phone:tr.find('[data-elem-name="phone"]').text()
			} 
			Helper.wait('on');
			$.ajax({
				url:'/phonebook/update',
				type:'post',
				data:dataSet,
				success:function(data) {
					Helper.wait('off');
					if(data.success) {
						alert('Phonebook updated.');
					}
					else {
						alert('Unable to update phonebook.');
					}
				}
				
			})
		
		});
	$(document).on('mouseenter','#entry-list tr',function(){
		$(this).find('.btn-danger').show();
	});
	
	$(document).on('mouseleave','#entry-list tr',function(){
		$(this).find('.btn-danger').show();
	});
	
	$(document).on('click','.remove-entry', function(){
		var elem = $(this);
		if(confirm('Are you sure')) {
			Helper.wait('on');
			$.get('/phonebook/remove?id='+$(this).data('id'),function(data) {
				Helper.wait('off');
				if(data.success) {
					elem.closest('tr').remove();
				}
			});	
		}
		
	});
	
	function validate() {
		return true;
	}
	$(document).on('click','.remove-entry-form', function(){
		$(this).closest('tr.entry-add-form').slideUp().remove();	
	});
	
	/*$(document).on('click','.remove-entry-form',function() {
		$(this).closest('tr.entry-add-form').slideUp(function() {
			$(this).remove();
		});
	);*/
	
	$('#search-form').validate({
		submitHandler:function(form) {
			$(form).ajaxSubmit({
				beforeSubmit:function(){
					Helper.wait('on');
					},
				success:function(data){
					Helper.wait('off');
					if(data.success) {
						$('#entry-list').slideUp().find('tbody').html(data.list_html);
						$('#entry-list').slideDown();
					}
				}
				});
		}
	});
	
	$(document).on('click','.add-entry',function(){
		var tr = $(this).closest('tr');
		if(validate(tr)) {
			var dataSet = {
					name:tr.find('[name="name"]').val(),
					phone:tr.find('[name="phone"]').val()
			} 
			Helper.wait('on');
			$.ajax({
				url:'/phonebook/add',
				type:'post',
				data:dataSet,
				success:function(data) {
					Helper.wait('off');
					if(data.success) {
						d = new Date();
						dataSet.date = data.date;
						dataSet.id = data.id;
						appendRow(dataSet, tr);
					}
					else {
						alert(data.message);
					}
					
				}
				
			})
		}
	});
	function appendRow(data, row) {
		var html = '<tr data-entry-id="'+data.id+'"><td><p contenteditable="true" class="changeable" data-elem-name="name">'+data.name+'</p></td>'+
	    '<td><p contenteditable="true" class="changeable" data-elem-name="phone">'+data.phone+'</p> </td>'+
	    '<td><button class="btn btn-danger pull-right btn-xs remove-entry" data-id="'+data.id+'"><i class="fa fa-trash-o"></i></button><small><i class="fa fa-clock-o"></i> added on '+data.date+'</small></td></tr>'
		row.replaceWith(html);
	}
	
	$('.add-contact').click(function() {
		var table = $('#entry-list');
		var form = $('<tr class="entry-add-form"><td><input type="text" name="name"></td>'+
			   '<td><input type="text" name="phone"></td>'+
			    '<td><button class="btn btn-primary btn-xs add-entry"><i class="fa fa-plus"></i> add</button> <button class="btn btn-default btn-xs remove-entry-form">cancel</button></td></tr>');
		if(table.find('tr.entry-add-form').children().length == 0 ) {
			table.prepend(form);
		}
	});
	
});
var Helper = {
		wait:function(status) {
			var elem = $('#waiting-div');
			if(status == 'on' ) { elem.fadeIn(); }
			if(status == 'off' ) { elem.fadeOut(); }
			
		},
		closeModal:function(){
			$('#modal-window').modal('hide');
		},
		dialog:function(element){
			Helper.wait('on');
			$modal = $('#modal-window');
			var href = element.prop('href');
			$.get(href, function(data){
				Helper.wait('off');
				if(data.success) {
					$modal.html('').html(data.html);
					$modal.modal();
					
				}
			});
		}
}
