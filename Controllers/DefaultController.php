<?php
use Portus\Controller;
use Portus\Factory\Helper;
class DefaultController extends Controller {
	
	public function index() {
		$documentManager =  $this->getDocumentManager();
		$qb = $documentManager->getRepository('Phonebook\\Entry')->createQueryBuilder();
		$list = $qb->sort('addedOn', -1)->getQuery()->execute(); 
		
		$__page =  (object)array('title' => 'Phonebook ',
				'sections' => array(APP_TEMPLATE_PATH.'/section_phonebook.list.php'),
				'assets' => array(
						array('SCRIPT', APP_CDN_PATH.'/js/jquery.validate.min.js','HEAD'),
						array('SCRIPT', APP_CDN_PATH.'/js/jquery.form.min.js','HEAD'),
				)
		
		);
		require_once APP_TEMPLATE_PATH.'/base.php';
		
	}
	
}