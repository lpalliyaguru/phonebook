<?php
use Portus\Controller;
use Phonebook\Entry;
use Portus\Factory\Helper;
use Portus\File\Bucket;
use Portus\File\File;
use Portus\Log\Log;
class PhonebookController extends Controller {
	
	public function search() {
		try {
			header('Content-type:application/json');
			$term = $this->hasParam('term') ? $this->getParam('term') : null;
			$documentManager = $this->getDocumentManager();
			$queryBuilder = $documentManager->getRepository('Phonebook\\Entry')->createQueryBuilder();
			$queryBuilder->addAnd(
					$queryBuilder->expr()
					->addOr($queryBuilder->expr()->field('name')->equals(new \MongoRegex('/.*'.$term.'.*/i')))
				);
			$list = $queryBuilder->sort('addedOn', -1)->getQuery()->execute();
			$html = $this->getListHtml($list);
			echo json_encode(array('success' => true, 'message' => '','list_html' => $html));
			
		}
		catch(\MongoException $e) {
			echo json_encode(array('success' => false, 'message' => 'Unexpected error.'));
		}
		catch(\Exception $e) {
			echo json_encode(array('success' => false, 'message' => $e->getMessage()));
		}	
	}
	
	public function import() {
		try {
			header('Content-type:application/json');
			$file = $this->getFiles('file');
			if(is_null($file)) { throw new Exception('Not a valid file');  }
			$bucket = new Bucket(APP_FILE_UPLOAD_PATH);
			$documentManager = $this->getDocumentManager();

			if(!is_null($file)) {
				$file = (object)$file;
				$storableFile = new File($file);
				$storableFile->setId(File::getUniqueId());
				if($storableFile->getMime() != 'text/csv') { throw new Exception('Only accepting csv format'); }
				$bucket->addObject($storableFile);
				$bucket->flush();
				$csvFileHandler = fopen(APP_FILE_UPLOAD_PATH . '/' . $storableFile->getStorableName() ,"r");
				$list = [];
				$i = 0;
				while(! feof($csvFileHandler)) {
					$line = fgetcsv($csvFileHandler);
					//Log::write(json_encode($line));
					if($i != 0 && is_array($line)) {
						list($name, $phone) = $line;
						$entry = $documentManager->getRepository('Phonebook\\Entry')->findOneBy(array('phone' => $phone));
						if($entry instanceof Entry) {
							
							$entry->setName($name);
						}
						else {
							$entry = new Entry();
							$entry->setName($name)->setPhone($phone)->setAddedDate(new DateTime());
						}
						$documentManager->persist($entry);
					}
					$i ++;
				}
				$documentManager->flush();
				fclose($csvFileHandler);
			}
			$qb = $documentManager->getRepository('Phonebook\\Entry')->createQueryBuilder();
			$list = $qb->sort('addedOn', -1)->getQuery()->execute();
			$html = $this->getListHtml($list);
			echo json_encode(array('success' => true, 'message' => 'Phonebook Updated.','list_html' => $html));
		}
		catch (Exception $e) {
			echo json_encode(array('success' => false,'message' => $e->getMessage()));			
		}
	}
	
	
	private function getListHtml($list) {
		
		ob_start();
		include_once APP_TEMPLATE_PATH . '/section_phonebook.entry.list.php';
		$buffer_content = ob_get_contents();
		ob_end_clean();
		return $buffer_content;
	}
	
	public function show_import_box() {
		try {
			header('Content-type:application/json');
			ob_start();
			include_once APP_TEMPLATE_PATH . '/box_import.entries.php';
			$bufffer_content = ob_get_contents();
			ob_end_clean();
			echo json_encode(array('success' => true, 'message' => '', 'html' => $bufffer_content));
		}
		catch (Exception $e) {
			echo json_encode(array('success' => false));			
		}
	}
	
	public function export() {
		try {
			$documentManager = $this->getDocumentManager();
			$qb = $documentManager->getRepository('Phonebook\\Entry')->createQueryBuilder();
			$list = $qb->sort('addedOn', -1)->getQuery()->execute();
			ob_end_clean();
			ob_start();
			header('Cache-control: private');
			header('Pragma: private');
			header('Content-Description: File Transfer');
			header('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
			header('Content-Type: application/force-download');
			header('Content-Type: application/octet-stream', false);
			header('Content-Type: application/download', false);
			header('Content-Disposition: attachment; filename=phonebook.csv');
			header('Content-Transfer-Encoding: binary');
			echo implode(",",array('Name','Phone')).PHP_EOL;
			foreach ($list as $entry) {
				echo implode(",", array($entry->getName(), $entry->getPhone()
				)).PHP_EOL;
			}
			
			$datas = ob_get_contents();
			ob_end_clean();
			echo $datas;
			exit();
			
		}
		catch(\MongoException $e) {
			echo json_encode(array('success' => false, 'message' => 'Unexpected error.'));
		}
		catch(\Exception $e) {
			echo json_encode(array('success' => false, 'message' => $e->getMessage()));
		}
	}

	public function remove() {
		try {
			header('Content-type:application/json');
			$id = $this->hasParam('id') && $this->getParam('id') != '' ? $this->getParam('id') : null;
			$documentManager = $this->getDocumentManager();
			$entry = $documentManager->find('Phonebook\\Entry', $id);
			if(!$entry instanceof Entry) { throw new \Exception('Not a valid entry'); } 
			$documentManager->remove($entry);
			$documentManager->flush();
			echo json_encode(array(
					'success' => true, 
					'message' => 
					'Phonebook updated', 
					'date' => Helper::time_offset(new DateTime())
			));
				
		}
		catch(\MongoException $e) {
			echo json_encode(array('success' => false, 'message' => 'Unexpected error.'));
		}
		catch(\Exception $e) {
			echo json_encode(array('success' => false, 'message' => $e->getMessage()));
		}	
	}
	
	public function update() {
		try {
			header('Content-type:application/json');
			$name = $this->hasParam('name') && $this->getParam('name') != '' ? $this->getParam('name') : null;
			$phone = $this->hasParam('phone') && $this->getParam('phone') != '' ? $this->getParam('phone') : null;
			$id = $this->hasParam('id') && $this->getParam('id') != '' ? $this->getParam('id') : null;
			
			//if(is_null($name)) { throw new \Exception('Name cannot be empty'); }
			//if(is_null($phone)) { throw new \Exception('Phone cannot be empty'); }
			$documentManager = $this->getDocumentManager();
			$entry = $documentManager->find('Phonebook\\Entry', $id);
			if(!$entry instanceof Entry) { throw new \Exception('Not a valid entry'); }
			
			if(!is_null($name)) { $entry->setName($name); }
			if(!is_null($phone)) { $entry->setPhone($phone); }
			$documentManager->persist($entry);
			$documentManager->flush();
			echo json_encode(array(
					'success' => true,
					'message' => 'Phonebook updated',
					'date' => Helper::time_offset(new DateTime()),
					'id' => $entry->getId()
			));
				
		}
		catch(\MongoException $e) {
			echo json_encode(array('success' => false, 'message' => 'Unexpected error.'));
		}
		catch(\Exception $e) {
			echo json_encode(array('success' => false, 'message' => $e->getMessage()));
		}
	}
	
	public function add() {
		try {
			header('Content-type:application/json');
			$name = $this->hasParam('name') && trim($this->getParam('name')) != '' ? $this->getParam('name') : null;
			$phone = $this->hasParam('phone') && trim($this->getParam('phone')) != '' ? $this->getParam('phone') : null;
			if(is_null($name)) { throw new \Exception('Name cannot be empty'); }
			if(is_null($phone)) { throw new \Exception('Phone cannot be empty'); }
			$entry = new Entry();
			$entry->setName($name)->setPhone($phone)->setAddedDate(new DateTime());
			$documentManager = $this->getDocumentManager();
			$documentManager->persist($entry);
			$documentManager->flush();
			echo json_encode(array(
					'success' => true, 
					'message' => 'Phonebook updated', 
					'date' => Helper::time_offset(new DateTime()),
					'id' => $entry->getId()
			));
			
		}
		catch(\MongoException $e) {
			echo json_encode(array('success' => false, 'message' => 'Unexpected error.'));
		}
		catch(\Exception $e) {
			echo json_encode(array('success' => false, 'message' => $e->getMessage()));
		}
	}
	
}